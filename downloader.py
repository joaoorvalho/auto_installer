# coding:utf-8
"""
Get installer from download page

Joao Orvalho
"""
from subprocess import (PIPE, Popen)
from modules.web_crawler.index_of_pages import crawler


def run_command(command):
    '''
    Invoke command as a new system process and return its output.
    '''
    return Popen(command, stdout=PIPE, shell=True).stdout.read()

source_link= "http://products.kaspersky-labs.com/english/homeuser/kis2018/"
file_link= crawler.run(source_link)
file_name= file_link.replace(source_link, "")
result = run_command('wget '+file_link) # download


run_command(file_name + ' /s') #silent installation

# for a in BeautifulSoup(str(download_box)).findAll('a',href=True):
#         print "Found the URL:", a['href']

# for line in page.content:
#     print line

