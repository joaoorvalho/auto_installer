# coding:utf-8

"""
Get installer from download page

Joao Orvalho
"""

from BeautifulSoup import BeautifulSoup
import requests


def read_website_content(url):
    """
    Return parsed HTML
    :param url: Website URL
    :return: Website content
    """
    page = requests.get(url)
    #tree = html.fromstring(page.content)
    html = page.content
    return BeautifulSoup(html)

def get_download_link(parsed_html):
    """
    Get download link
    :param parsed_html: Parsed HTML
    :return: link
    """
    download_box=  parsed_html.body.find('div', attrs={'class':'product-download'})
    link= BeautifulSoup(str(download_box)).findAll('a',href=True)[0]['href']
    return link

def run(url):
    """
    Run module
    :param url: Website URL
    :return: Link of the last version of the program available (full version)
    """
    parsed_html = read_website_content(url)
    link = get_download_link(parsed_html)
    print link

if __name__ == '__main__':
    run('https://www.kaspersky.pt/free-trials/home-security/internet-security')