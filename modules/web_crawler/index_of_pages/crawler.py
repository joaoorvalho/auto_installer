# coding:utf-8
"""
Get installer from download page

Joao Orvalho
"""

import requests
from time import strptime
import re

def filter_more_than_100mb(list_of_lines):
    """
    Get only lines with files that have more than 100 MB
    :param list_of_lines: list of lines
    :return: list of lines with whitch have files with more than 100 MB
    """
    lines_more_than_100mb = []
    for line in list_of_lines:
        if (int(line[4]) > 100000000): # more than 100000000 bytes (> 100 megabytes)
            lines_more_than_100mb.append(line)
    return lines_more_than_100mb

def sort_lines_by_date(list_of_lines):
    """
    Sort lines by date
    :param list_of_lines: list of lines
    :return: lines sorted by line
    """
    import datetime
    return sorted(list_of_lines, key=lambda x: datetime.datetime.strptime( ( x[0]+ '-' +str(strptime(x[1],'%b').tm_mon)+'-'+x[2]+ ' '+x[3]) , '%d-%m-%Y %H:%M:%S'), reverse=True)


def read_website_content(url):
    """
    Read website content
    :param url: Website URL
    :return: list of lines with the website content
    """
    r = requests.get(url, stream=True)
    web_site_content_list = []
    for line in r.iter_lines():
        if line:
            line_split_space = line.split(" ")
            line_no_blank_spaces= filter(None, line_split_space)
            if len(line_no_blank_spaces) == 7:
                web_site_content_list.append(line_no_blank_spaces)
    return web_site_content_list


def run(url):
    """
    Run module
    :param url: Website URL
    :return: Link of the last version of the program available (full version)
    """
    web_site_content_list = read_website_content(url)
    lines_more_than_100mb= filter_more_than_100mb(web_site_content_list)
    newest_line = sort_lines_by_date (lines_more_than_100mb)[0]
    #print newest_line
    return url + re.findall('"(.*?)"', newest_line[6])[0] # get value between quotation marks

if __name__ == '__main__':
    print run('http://products.kaspersky-labs.com/english/homeuser/kis2018/')